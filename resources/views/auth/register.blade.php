<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registrasi</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <form action="/welcome" method="POST">
    @csrf
    <h2>Sign Up Form</h2>
    <label>First Name:</label><br>
    <input type="text" name="firstname"><br><br>
    <label>Last Name:</label><br>
    <input type="text" name="lastname"><br><br>
    <label for="gender">Gender:</label><br><br>
    <input type="radio" name="Male" id="Male">Male<br>
    <input type="radio" name="Female" id="Female">Female<br>
    <input type="radio" name="Other" id="Other">Other<br><br>
    <label for="Nationality">Nationality:</label><br><br>
    <select>
        <option value="Indonesia">Indonesian</option><br>
        <option value="Malaysia">Singapoeran</option><br>
        <option value="Thailand">Malaysian</option><br>
        <option value="Myanmar">Australian</option><br><br>
    </select>
    <br><br>
    <label for="Language_Spoken">Language Spoken:</label><br><br>
    <input type="checkbox" name="Bhs_Indonesia" id="Bahasa Indonesia">Bahasa Indonesia<br>
    <input type="checkbox" name="English" id="English">English <br>
    <input type="checkbox" name="Other" id="Other">Other <br><br>
    <label for="Bio">Bio</label><br><br>
    <textarea name="Bio" id="Bio" cols="30" rows="10"></textarea><br>
    <input type="submit" value="Sign Up">
    </form>

    
</body>
</html>